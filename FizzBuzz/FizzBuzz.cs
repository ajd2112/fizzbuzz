﻿namespace FizzBuzz
{
    public class FizzBuzz
    {
        public string FizzBuzzify(int input)
        {
            if (input % 15 == 0)
                return "FizzBuzz";

            if (input % 3 == 0)
                return "Fizz";

            return input % 5 == 0 ? "Buzz" : input.ToString();
        }
    }
}
