﻿using System;

namespace FizzBuzz
{
    public class FizzBuzzWriter
    {
        public void Write(int[] inputs)
        {
            var fizzBuzz = new FizzBuzz();
            foreach (var input in inputs)
            {
                Console.WriteLine(fizzBuzz.FizzBuzzify(input));
            }
        }
    }
}
