﻿using System;

namespace FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            var fizzBuzzer = new FizzBuzzWriter();
            fizzBuzzer.Write(new[] { 1, 3, 5, 14, 15, 16 });
            Console.ReadLine();
        }
    }
}
